#pragma once

#include "targetver.h"

#include <tchar.h>

#include <cassert>
#include <compare>
#include <cstddef>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <stack>
#include <string>
#include <string_view>
#include <utility>
#include <vector>

#include <sal.h>

#include <gsl/narrow>
#include <gsl/pointers>
#include <gsl/span>
