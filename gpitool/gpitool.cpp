#include "stdafx.h"

// TODO(me): do not assume processor with little endian byte order

#pragma warning(disable: 4018)	// '>=': signed/unsigned mismatch
#pragma warning(disable: 4200)	// nonstandard extension used : zero-sized array in struct/union
#pragma warning(disable: 4389)	// '==': signed/unsigned mismatch
#pragma warning(disable: 26490)	// Don't use reinterpret_cast (type.1).
#pragma warning(disable: 26711)	// Potential overflow of span using expression 'gsl::span<...,-1>::subspan'
namespace
{
#pragma pack(push, 1)
	struct Header {
		uint16_t	tag;
		uint16_t	flags;
		uint32_t	len;		// Of data after header
//		uint32_t	len2[];		// Present if flags == 0x0008, array length is 1 element

		[[nodiscard]] std::size_t GetHeaderLength() const noexcept
		{
			if (flags == 0)
			{
				return sizeof(Header);
			}
			else {
				assert(flags == 0x0008);
				return sizeof(Header) + sizeof(uint32_t);
			}
		}

		[[nodiscard]] gsl::span<const std::byte, gsl::dynamic_extent> AsSpan() const noexcept
		{
			return { reinterpret_cast<const std::byte*>(this), GetHeaderLength() + len };
		}

		[[nodiscard]] gsl::span<const std::byte, gsl::dynamic_extent> GetData() const noexcept
		{
			return AsSpan().subspan(GetHeaderLength());
		}

		[[nodiscard]] std::size_t GetLen2() const noexcept
		{
			[[gsl::suppress(bounds.1)]]
			return std::size_t{ *reinterpret_cast<const uint32_t*>(this + 1) };
		}

		[[nodiscard]] gsl::not_null<const Header *> GetNext() const noexcept
		{
			auto const s{ AsSpan() };
			[[gsl::suppress(bounds.1)]]
			return gsl::make_not_null(reinterpret_cast<const Header*>(s.data() + s.size()));
		}
	};

	struct StrWithLen {
		uint16_t	len;
//		char		str[/*len*/];

		[[nodiscard]] std::size_t GetTotalLength() const noexcept { return sizeof *this + len; }
		[[nodiscard]] std::string_view AsStringView() const noexcept {
			[[gsl::suppress(bounds.1)]]
			return std::string_view{ reinterpret_cast<const char*>(this + 1), len };
		}
		[[nodiscard]] const void *GetEnd() const noexcept {
			[[gsl::suppress(bounds.1)]]
			return reinterpret_cast<const char*>(this + 1) + len;
		}
	};
	_Ret_ std::ostream &operator <<(_In_ std::ostream &os, const StrWithLen &s)
	{
		return os << s.AsStringView();
	}

	struct LocalizedStrWithLen {
		uint32_t		len;
		char			language[2];
		StrWithLen		str;

		void Check() const noexcept {
			assert(len == sizeof language + str.GetTotalLength());
		}
		[[nodiscard]] std::size_t GetTotalLength() const noexcept { return sizeof *this + str.len; }
		[[nodiscard]] std::string_view GetLanguage() const noexcept {
			[[gsl::suppress(bounds.3)]]
			return std::string_view{ language, sizeof language };
		}
		[[nodiscard]] std::string_view GetStringView() const noexcept { return str.AsStringView(); }
		[[nodiscard]] const void *GetEnd() const noexcept { return str.GetEnd(); }
	};
	_Ret_ std::ostream &operator <<(_In_ std::ostream &os, const LocalizedStrWithLen &s)
	{
		return os << s.GetLanguage() << ':' << s.GetStringView();
	}
#pragma pack(pop)

	size_t g_offsetPrefixString{ 0 };

	[[nodiscard]] std::string GetPrefixString()
	{
		return std::string(2 * g_offsetPrefixString, '.');
	}

	struct LatLon
	{
		uint32_t value;	// Semi circles

		[[nodiscard]] auto operator<=>(const LatLon&) const noexcept = default;

		[[nodiscard]] constexpr double ToDegrees() const { return (static_cast<double>(value) / static_cast<double>(1U << 31)) * 180.; }
	};
	_Ret_ std::ostream &operator <<(_In_ std::ostream &os, const LatLon &value)
	{
//TODO(me) Print W and S instead of N and E.
		auto const d{ value.ToDegrees() };
		return os << std::dec << static_cast<int>(d) << '�' << std::fixed << std::setprecision(3) << (d - static_cast<int>(d)) * 60;
	}

	void TraverseHeaders(_In_ gsl::span<const std::byte, gsl::dynamic_extent> block);

	void TraverseTag0000(_In_ gsl::span<const std::byte, gsl::dynamic_extent> block)
	{
		assert(block.size() >= sizeof(Header));
		auto const pHeader{ gsl::make_not_null(reinterpret_cast<const Header*>(block.data())) };
		assert(block.size() >= pHeader->GetHeaderLength());

#pragma pack(push, 1)
		struct Data1 {
			char		grmrec0[7];
			char		f2_;		// Garmin '1', POILoader '0', GSAK '0'
			__time32_t	time;
			uint16_t	f4_;
			StrWithLen	name;

			constexpr size_t size() const noexcept { return sizeof(*this) + name.len; }
		};
#pragma pack(pop)

		auto const blockdata{ pHeader->GetData() };
		auto const pData1 = gsl::make_not_null(reinterpret_cast<const Data1*>(blockdata.data()));
		if (pHeader->flags == 0) {
			assert(blockdata.size() >= sizeof(*pData1));
			assert(blockdata.size() == pData1->size());
			assert(pData1->f2_ == '0');
			assert(pData1->f4_ == 0x0000);
		}
		else {
			assert(pHeader->flags == 0x0008);
			assert(pHeader->GetLen2() >= sizeof(*pData1));
			assert(pHeader->GetLen2() == pData1->size());
			assert(pData1->f2_ == '1');
			assert(pData1->f4_ == 0x0003);
		}
		[[gsl::suppress(bounds.3)]]
		assert((std::string_view{ pData1->grmrec0, sizeof pData1->grmrec0 } == std::string_view{ "GRMREC0" }));
		const __time32_t time{ pData1->time + 631065600 };
		struct tm tm {};
		const errno_t err{ _localtime32_s(&tm, &time) };
		assert(err == 0);
		char strtime[32];
		[[gsl::suppress(bounds.3)]]
		strftime(strtime, sizeof(strtime), "%Y/%m/%d %H:%M:%S", &tm);
		[[gsl::suppress(bounds.3)]]
		std::cout << std::hex << std::setfill('0')
			<< " f2=" << pData1->f2_
			<< " time=" << strtime
			<< " f4=" << std::setw(4) << pData1->f4_
			<< " name=" << pData1->name.AsStringView() << '\n';

		if (pHeader->flags == 0x0008) {
			// Now walk over all sub items
			++g_offsetPrefixString;
			TraverseHeaders({ static_cast<const std::byte*>(pData1->name.GetEnd()), block.data() + block.size() });
			--g_offsetPrefixString;
		}
	}

	void TraverseTag0001(_In_ gsl::span<const std::byte, gsl::dynamic_extent> block)
	{
		assert(block.size() >= sizeof(Header));
		auto const pHeader{ gsl::make_not_null(reinterpret_cast<const Header*>(block.data())) };
		assert(block.size() >= pHeader->GetHeaderLength());
		assert(block.size() >= pHeader->GetHeaderLength() + pHeader->GetData().size());

#pragma pack(push, 1)
		struct Data1 {
			char		POI___0[7];
			uint8_t		f3_;
			uint16_t	codepage;
			uint16_t	f5_;	// 0 or following tag

			constexpr size_t size() const noexcept { return sizeof(*this); }
		};
#pragma pack(pop)

		auto const blockdata{ pHeader->GetData() };
		auto const pData1 = gsl::make_not_null(reinterpret_cast<const Data1*>(blockdata.data()));
		if (pHeader->flags == 0x0000) {
			// GSAK or POILoader, POIs displayed directly or under "Benutzer-POIs"
			assert(blockdata.size() >= sizeof(*pData1));
			assert(blockdata.size() == pData1->size());
			assert(pData1->f3_ == '0');
			assert(pData1->f5_ == 0x0000);
		}
		else {
			// Garmin, POIs displayed under name given in following tag 0011
			assert(pHeader->flags == 0x0008);
			assert(pHeader->GetLen2() >= sizeof(*pData1));
			assert(pHeader->GetLen2() == pData1->size());
			assert(blockdata.size() > pHeader->GetLen2());
			assert(pData1->f3_ == '1');
			assert(pData1->f5_ == 0x0011);
		}
		assert(pData1->POI___0[0] == 'P');
		assert(pData1->POI___0[1] == 'O');
		assert(pData1->POI___0[2] == 'I');
		assert(pData1->POI___0[3] == '\0');
		assert(pData1->POI___0[4] == '\0');
		assert(pData1->POI___0[5] == '\0');
		assert(pData1->POI___0[6] == '0');
		assert(pData1->codepage == 0x04e4);
		std::cout << std::hex << std::setfill('0');
		std::cout << " f3=" << std::setw(2) << static_cast<unsigned>(pData1->f3_)
			<< " cp=" << std::dec << std::setw(4) << pData1->codepage
			<< " f5=" << std::hex << std::setw(4) << pData1->f5_ << '\n';

		if (pHeader->flags == 0x0008) {
			// Now walk over all sub items
			++g_offsetPrefixString;
			TraverseHeaders(blockdata.subspan(pData1->size()));
			--g_offsetPrefixString;
		}
	}

	void TraverseTag0002(_In_ gsl::span<const std::byte, gsl::dynamic_extent> block)
	{
		assert(block.size() >= sizeof(Header));
		auto const pHeader{ gsl::make_not_null(reinterpret_cast<const Header*>(block.data())) };
		assert(block.size() >= pHeader->GetHeaderLength());
		assert(block.size() >= pHeader->GetHeaderLength() + pHeader->GetData().size());

#pragma pack(push, 1)
		struct Data1 {
			LatLon				latitude;
			LatLon				longitude;
			uint8_t				c1;
			uint16_t			f4;	// 0x0200 for Garmin, 0x0000 for POILoader and GSAK, 0x0100 for POILoader (Test mit gpx)
			LocalizedStrWithLen	str;

			size_t size() const noexcept { return sizeof(*this) - sizeof(str) + str.GetTotalLength(); }
		};
#pragma pack(pop)

		auto const blockdata{ pHeader->GetData() };
		auto const pData1 = gsl::make_not_null(reinterpret_cast<const Data1*>(blockdata.data()));
		if (pHeader->flags == 0x0000)
		{
		}
		else
		{
			assert(pHeader->flags == 0x0008);
			assert(blockdata.size() >= pHeader->GetLen2());
			assert(pHeader->GetLen2() >= sizeof(*pData1));
			assert(pHeader->GetLen2() == pData1->size());
		}
		assert(pData1->c1 == 1);
		assert(pData1->f4 == 0 || pData1->f4 == 0x0100 || pData1->f4 == 0x0200);
		pData1->str.Check();

		std::cout << " coord=N" << pData1->latitude << " E" << pData1->longitude
			<< " f4=" << std::hex << std::setw(4) << pData1->f4 << " name=" << pData1->str << '\n';

		// Now walk over all sub items
		++g_offsetPrefixString;
		TraverseHeaders({ static_cast<const std::byte*>(pData1->str.GetEnd()), block.data() + block.size() });
		--g_offsetPrefixString;
	}

	void TraverseTag0003(_In_ gsl::span<const std::byte, gsl::dynamic_extent> block)
	{
		assert(block.size() >= sizeof(Header));
		auto const pHeader{ gsl::make_not_null(reinterpret_cast<const Header*>(block.data())) };
		assert(block.size() >= pHeader->GetHeaderLength());
		assert(block.size() >= pHeader->GetHeaderLength() + pHeader->GetData().size());

#pragma pack(push, 1)
		struct Data1 {
			// Proximity alarm
			uint16_t distance;	// in m
			uint16_t speed;		// in 100 m/s
			uint8_t	f1_[8];

			constexpr size_t size() const noexcept { return sizeof(*this); }
		};
#pragma pack(pop)

		auto const blockdata{ pHeader->GetData() };
		auto const pData1 = gsl::make_not_null(reinterpret_cast<const Data1*>(blockdata.data()));
		assert(pHeader->flags == 0);
		assert(blockdata.size() >= sizeof(*pData1));
		assert(blockdata.size() == pData1->size());
		assert(pData1->f1_[0] == 0x00);
		assert(pData1->f1_[1] == 0x01);
		assert(pData1->f1_[2] == 0x00);
		assert(pData1->f1_[3] == 0x01);
		assert(pData1->f1_[4] == 0x01);
		assert(pData1->f1_[5] == 0x01);
		// 0x05: POILoader "Warnung, wenn Geschwindigkeit Warnungsgeschwindigkeit �berschreitet"
		// 0x04: POILoader "Warnung bei Ann�herung an einen POI"
		assert(pData1->f1_[6] == 0x04  || pData1->f1_[6] == 0x05);
		if (pData1->f1_[6] == 0x04)
		{
			assert(pData1->speed == 0);
		}
		if (pData1->f1_[6] == 0x05)
		{
			assert(pData1->speed != 0);
		}
		assert(pData1->f1_[7] == 0x10);
		std::cout << std::dec << " dist=" << pData1->distance << " speed=" << pData1->speed
			 << " f1_[6]=" << std::hex << std::setw(2) << static_cast<unsigned>(pData1->f1_[6])
			 << '\n';
	}

	void TraverseTag0004(_In_ gsl::span<const std::byte, gsl::dynamic_extent> block)
	{
		assert(block.size() >= sizeof(Header));
		auto const pHeader{ gsl::make_not_null(reinterpret_cast<const Header*>(block.data())) };
		assert(block.size() >= pHeader->GetHeaderLength());
		assert(block.size() >= pHeader->GetHeaderLength() + pHeader->GetData().size());

#pragma pack(push, 1)
		struct Data1 {
			uint16_t	folder;

			constexpr size_t size() const noexcept { return sizeof(*this); }
		};
#pragma pack(pop)

		auto const blockdata{ pHeader->GetData() };
		auto const pData1 = gsl::make_not_null(reinterpret_cast<const Data1*>(blockdata.data()));
		assert(pHeader->flags == 0);
		assert(blockdata.size() >= sizeof(*pData1));
		assert(blockdata.size() == pData1->size());
		std::cout << " folder=" << std::hex << std::setw(2) << pData1->folder
			<< '\n';
	}

	void TraverseTag0005(_In_ gsl::span<const std::byte, gsl::dynamic_extent> block)
	{
		assert(block.size() >= sizeof(Header));
		auto const pHeader{ gsl::make_not_null(reinterpret_cast<const Header*>(block.data())) };
		assert(block.size() >= pHeader->GetHeaderLength());
		assert(block.size() >= pHeader->GetHeaderLength() + pHeader->GetData().size());

		// Bitmap data
		assert(pHeader->flags == 0);
		std::cout << '\n';
	}

	void TraverseTag0006(_In_ gsl::span<const std::byte, gsl::dynamic_extent> block)
	{
		assert(block.size() >= sizeof(Header));
		auto const pHeader{ gsl::make_not_null(reinterpret_cast<const Header*>(block.data())) };
		assert(block.size() >= pHeader->GetHeaderLength());
		assert(block.size() >= pHeader->GetHeaderLength() + pHeader->GetData().size());

#pragma pack(push, 1)
		struct Data1 {
			uint16_t	folder;

			constexpr size_t size() const noexcept { return sizeof(*this); }
		};
#pragma pack(pop)

		auto const blockdata{ pHeader->GetData() };
		auto const pData1 = gsl::make_not_null(reinterpret_cast<const Data1*>(blockdata.data()));
		assert(pHeader->flags == 0);
		assert(blockdata.size() >= sizeof(*pData1));
		assert(blockdata.size() == pData1->size());
		std::cout << " folder=" << std::hex << std::setw(2) << pData1->folder
			<< '\n';
	}

	void TraverseTag0007(_In_ gsl::span<const std::byte, gsl::dynamic_extent> block)
	{
		assert(block.size() >= sizeof(Header));
		auto const pHeader{ gsl::make_not_null(reinterpret_cast<const Header*>(block.data())) };
		assert(block.size() >= pHeader->GetHeaderLength());
		assert(block.size() >= pHeader->GetHeaderLength() + pHeader->GetData().size());

#pragma pack(push, 1)
		struct Data1 {
			uint16_t			folder;
			// Same as in 0009::str (but seems to be no problem if different).
			LocalizedStrWithLen	str;

			size_t size() const noexcept { return sizeof(*this) - sizeof(str) + str.GetTotalLength(); }
		};
#pragma pack(pop)

		auto const blockdata{ pHeader->GetData() };
		auto const pData1 = gsl::make_not_null(reinterpret_cast<const Data1*>(blockdata.data()));
		assert(pHeader->flags == 0x0000 || pHeader->flags == 0x0008);
		if (pHeader->flags == 0x0008) {
			assert(blockdata.size() >= pHeader->GetLen2());
			assert(pHeader->GetLen2() >= sizeof(*pData1));
			assert(pHeader->GetLen2() == pData1->size());
		}
		assert(blockdata.size() >= sizeof(*pData1));
		assert(blockdata.size() >= pData1->size());
		pData1->str.Check();

		std::cout << " folder=" << std::hex << std::setw(4) << pData1->folder
			<< " str=" << pData1->str << '\n';
	}

	void TraverseTag0008(_In_ gsl::span<const std::byte, gsl::dynamic_extent> block)
	{
		assert(block.size() >= sizeof(Header));
		auto const pHeader{ gsl::make_not_null(reinterpret_cast<const Header*>(block.data())) };
		assert(block.size() >= pHeader->GetHeaderLength());
		assert(block.size() >= pHeader->GetHeaderLength() + pHeader->GetData().size());

#pragma pack(push, 1)
		struct Data1 {
			LatLon			maxLatitude;
			LatLon			maxLongitude;
			LatLon			minLatitude;
			LatLon			minLongitude;
			uint32_t		c0_1;
			uint8_t			c1;
			uint16_t		f4;	// 0x0200 for Garmin, 0x0000 for POILoader and GSAK, 0x0100 for POILoader (Test mit gpx)

			constexpr size_t size() const noexcept { return sizeof(*this); }
		};
#pragma pack(pop)

		auto const blockdata{ pHeader->GetData() };
		auto const pData1 = gsl::make_not_null(reinterpret_cast<const Data1*>(blockdata.data()));
		assert(pHeader->flags == 0x0008);
		assert(blockdata.size() >= pHeader->GetLen2());
		assert(pHeader->GetLen2() >= sizeof(*pData1));
		assert(pHeader->GetLen2() == pData1->size());
//TODO(me) asserts korrigieren
		//assert(pData1->minLatitude <= pData1->maxLatitude);
		//assert(pData1->minLongitude <= pData1->maxLongitude);
		assert(pData1->c0_1 == 0);
		assert(pData1->c1 == 1);
		assert(pData1->f4 == 0  ||  pData1->f4 == 0x0100  ||  pData1->f4 == 0x0200);

		std::cout << " N" << pData1->minLatitude << " E" << pData1->minLongitude
			 << " - N" << pData1->maxLatitude << " E" << pData1->maxLongitude
			 << ' ' << std::hex << std::setw(4) << pData1->f4 << '\n';

		// Now walk over all sub items
		++g_offsetPrefixString;
		TraverseHeaders(blockdata.subspan(pData1->size()));
		--g_offsetPrefixString;
	}

	// Third tag in file with group name
	void TraverseTag0009(_In_ gsl::span<const std::byte, gsl::dynamic_extent> block)
	{
		assert(block.size() >= sizeof(Header));
		auto const pHeader{ gsl::make_not_null(reinterpret_cast<const Header*>(block.data())) };
		assert(block.size() >= pHeader->GetHeaderLength());
		assert(block.size() >= pHeader->GetHeaderLength() + pHeader->GetData().size());

#pragma pack(push, 1)
		struct Data1 {
			// Bei Garmin Name unter Find/Extras/xxx/. Bei POILoader und GSAK fehlt xxx oder wird durch "Benutzer-POIs" ersetzt.
			LocalizedStrWithLen	str;

			size_t size() const noexcept { return sizeof(*this) - sizeof(str) + str.GetTotalLength(); }
		};
#pragma pack(pop)
		auto const blockdata{ pHeader->GetData() };
		auto const pData1 = gsl::make_not_null(reinterpret_cast<const Data1*>(blockdata.data()));
		assert(blockdata.size() >= sizeof(*pData1));
		assert(blockdata.size() >= pData1->size());
		pData1->str.Check();

		std::cout << " str=" << pData1->str << '\n';

		if (pHeader->flags == 0x0008) {
			assert(pHeader->GetLen2() > pData1->str.GetTotalLength()  &&  pHeader->GetLen2() < blockdata.size());

			// Now walk over all sub items
			++g_offsetPrefixString;
			TraverseHeaders(blockdata.first(pHeader->GetLen2()).subspan(pData1->str.GetTotalLength()));
			--g_offsetPrefixString;

			std::cout << GetPrefixString() << "0009, second part\n";

			// Now walk over all sub items
			++g_offsetPrefixString;
			TraverseHeaders(blockdata.subspan(pHeader->GetLen2()));
			--g_offsetPrefixString;
		}
		else {
			// Now walk over all sub items
			++g_offsetPrefixString;
			TraverseHeaders(blockdata.subspan(pData1->str.GetTotalLength()));
			--g_offsetPrefixString;
		}
	}

	void TraverseTag000A(_In_ gsl::span<const std::byte, gsl::dynamic_extent> block)
	{
		assert(block.size() >= sizeof(Header));
		auto const pHeader{ gsl::make_not_null(reinterpret_cast<const Header*>(block.data())) };
		assert(block.size() >= pHeader->GetHeaderLength());
		assert(block.size() >= pHeader->GetHeaderLength() + pHeader->GetData().size());

#pragma pack(push, 1)
		struct Data1 {
			LocalizedStrWithLen	str;

			size_t size() const noexcept { return sizeof(*this) - sizeof(str) + str.GetTotalLength(); }
		};
#pragma pack(pop)

		auto const blockdata{ pHeader->GetData() };
		auto const pData1 = gsl::make_not_null(reinterpret_cast<const Data1*>(blockdata.data()));
		assert(pHeader->flags == 0);
		assert(blockdata.size() >= sizeof(*pData1));
		assert(blockdata.size() == pData1->size());
		pData1->str.Check();

		std::cout << " cmt/desc=" << pData1->str << '\n';
	}

	void TraverseTag000B(_In_ gsl::span<const std::byte, gsl::dynamic_extent> block)
	{
		assert(block.size() >= sizeof(Header));
		auto const pHeader{ gsl::make_not_null(reinterpret_cast<const Header*>(block.data())) };
		assert(block.size() >= pHeader->GetHeaderLength());
		assert(block.size() >= pHeader->GetHeaderLength() + pHeader->GetData().size());

#pragma pack(push, 1)
		struct Data1 {
			uint16_t	flags;

			constexpr size_t size() const noexcept { return sizeof(*this); }
		};
#pragma pack(pop)

		auto blockdata{ pHeader->GetData() };
		auto const pData1{ gsl::not_null{ reinterpret_cast<const Data1*>(blockdata.data()) } };
		assert(blockdata.size() >= sizeof(*pData1));
		assert(blockdata.size() >= pData1->size());
		if (pHeader->flags == 0x0008) {
			assert(pHeader->GetLen2() == 0x0002 || pHeader->GetLen2() == blockdata.size());
		}
		assert(pData1->flags != 0);
		blockdata = blockdata.subspan(pData1->size());

		assert((pData1->flags & ~0x003F) == 0);
		if ((pData1->flags & 0x0001) != 0) {
			auto const str{ gsl::not_null{ reinterpret_cast<const LocalizedStrWithLen*>(blockdata.data()) } };
			assert(blockdata.size() >= sizeof *str);
			assert(blockdata.size() >= str->GetTotalLength());
			str->Check();
			blockdata = blockdata.subspan(str->GetTotalLength());
			std::cout << " Stadt/Ort=" << *str;
		}
		if ((pData1->flags & 0x0002) != 0) {
			auto const str{ gsl::not_null{ reinterpret_cast<const LocalizedStrWithLen*>(blockdata.data()) } };
			assert(blockdata.size() >= sizeof *str);
			assert(blockdata.size() >= str->GetTotalLength());
			str->Check();
			blockdata = blockdata.subspan(str->GetTotalLength());
			std::cout << " Land=" << *str;
		}
		if ((pData1->flags & 0x0004) != 0) {
			auto const str{ gsl::not_null{ reinterpret_cast<const LocalizedStrWithLen*>(blockdata.data()) } };
			assert(blockdata.size() >= sizeof *str);
			assert(blockdata.size() >= str->GetTotalLength());
			str->Check();
			blockdata = blockdata.subspan(str->GetTotalLength());
			std::cout << " Bundesland/Provinz=" << *str;
		}
		if ((pData1->flags & 0x0008) != 0) {
			auto const str{ gsl::not_null{ reinterpret_cast<const StrWithLen*>(blockdata.data()) } };
			assert(blockdata.size() >= sizeof *str);
			assert(blockdata.size() >= str->GetTotalLength());
			blockdata = blockdata.subspan(str->GetTotalLength());
			std::cout << " PLZ=" << *str;
		}
		if ((pData1->flags & 0x0010) != 0) {
			auto const str{ gsl::not_null{ reinterpret_cast<const LocalizedStrWithLen*>(blockdata.data()) } };
			assert(blockdata.size() >= sizeof *str);
			assert(blockdata.size() >= str->GetTotalLength());
			str->Check();
			blockdata = blockdata.subspan(str->GetTotalLength());
			std::cout << " Stra�e=" << *str;
		}
		if ((pData1->flags & 0x0020) != 0) {
			auto const str{ gsl::not_null{ reinterpret_cast<const StrWithLen*>(blockdata.data()) } };
			assert(blockdata.size() >= sizeof *str);
			assert(blockdata.size() >= str->GetTotalLength());
			blockdata = blockdata.subspan(str->GetTotalLength());
			std::cout << " Hausnr=" << *str;
		}
		assert(blockdata.size() == 0);
		std::cout << '\n';
	}

	void TraverseTag000C(_In_ gsl::span<const std::byte, gsl::dynamic_extent> block)
	{
		assert(block.size() >= sizeof(Header));
		auto const pHeader{ gsl::make_not_null(reinterpret_cast<const Header*>(block.data())) };
		assert(block.size() >= pHeader->GetHeaderLength());
		assert(block.size() >= pHeader->GetHeaderLength() + pHeader->GetData().size());

#pragma pack(push, 1)
		struct Data1 {
			uint16_t	flags;

			constexpr size_t size() const noexcept { return sizeof(*this); }
		};
#pragma pack(pop)

		auto blockdata{ pHeader->GetData() };
		auto const pData1{ gsl::not_null{ reinterpret_cast<const Data1*>(blockdata.data()) } };
		assert(blockdata.size() >= sizeof(*pData1));
		assert(blockdata.size() >= pData1->size());
		if (pHeader->flags == 0x0008) {
			assert(pHeader->GetLen2() == 0x0002 || pHeader->GetLen2() == blockdata.size());
		}
		assert(pData1->flags != 0);
		blockdata = blockdata.subspan(pData1->size());

		assert((pData1->flags & ~0x0019) == 0);
		if ((pData1->flags & 0x0001) != 0) {
			auto const str = gsl::make_not_null(reinterpret_cast<const StrWithLen *>(blockdata.data()));
			assert(blockdata.size() >= sizeof *str);
			assert(blockdata.size() >= str->GetTotalLength());
			blockdata = blockdata.subspan(str->GetTotalLength());
			std::cout << " Tel=" << *str;
		}
		if ((pData1->flags & 0x0008) != 0) {
			auto const str = gsl::make_not_null(reinterpret_cast<const StrWithLen *>(blockdata.data()));
			assert(blockdata.size() >= sizeof *str);
			assert(blockdata.size() >= str->GetTotalLength());
			blockdata = blockdata.subspan(str->GetTotalLength());
			std::cout << " Mail=" << *str;
		}
		if ((pData1->flags & 0x0010) != 0) {
			auto const str = gsl::make_not_null(reinterpret_cast<const StrWithLen *>(blockdata.data()));
			assert(blockdata.size() >= sizeof *str);
			assert(blockdata.size() >= str->GetTotalLength());
			blockdata = blockdata.subspan(str->GetTotalLength());
			std::cout << " Web=" << *str;
		}
		assert(blockdata.size() == 0);
		std::cout << '\n';
	}

	void TraverseTag000D(_In_ gsl::span<const std::byte, gsl::dynamic_extent> block)
	{
		assert(block.size() >= sizeof(Header));
		auto const pHeader{ gsl::make_not_null(reinterpret_cast<const Header*>(block.data())) };
		assert(block.size() >= pHeader->GetHeaderLength());
		assert(block.size() >= pHeader->GetHeaderLength() + pHeader->GetData().size());

		//auto const blockdata{ pHeader->GetData() };
		//auto const pData1 = reinterpret_cast<const void*>(pHeader->GetData().data());
		assert(pHeader->flags == 0);
		// Bild!? (Exif, Adobe Photoshop)
		std::cout << '\n';
	}

	void TraverseTag000E(_In_ gsl::span<const std::byte, gsl::dynamic_extent> block)
	{
		assert(block.size() >= sizeof(Header));
		auto const pHeader{ gsl::make_not_null(reinterpret_cast<const Header*>(block.data())) };
		assert(block.size() >= pHeader->GetHeaderLength());
		assert(block.size() >= pHeader->GetHeaderLength() + pHeader->GetData().size());

#pragma pack(push, 1)
		struct Data1 {
			uint8_t	flags;

			constexpr size_t size() const noexcept { return sizeof(*this); }
		};
#pragma pack(pop)

		auto blockdata{ pHeader->GetData() };
		auto const pData1 = gsl::make_not_null(reinterpret_cast<const Data1*>(blockdata.data()));
		assert(pHeader->flags == 0);
		assert(blockdata.size() >= sizeof(*pData1));
		assert(blockdata.size() >= pData1->size());
		assert(pData1->flags != 0);
		blockdata = blockdata.subspan(pData1->size());

		assert((pData1->flags & ~0x0005) == 0);
		if ((pData1->flags & 0x0004) != 0) {
			std::cout << " 0x0004";		// Always set for Garmin, but never for POILoader and GSAK
		}
		if ((pData1->flags & 0x0001) != 0) {
			auto const &str = *reinterpret_cast<const LocalizedStrWithLen *>(blockdata.data());
			assert(blockdata.size() >= sizeof(str));
			assert(blockdata.size() >= str.GetTotalLength());
			str.Check();
			blockdata = blockdata.subspan(str.GetTotalLength());
			std::cout << " desc=" << str;
		}
		assert(blockdata.size() == 0);
		std::cout << '\n';
	}

	void TraverseTag000F(_In_ gsl::span<const std::byte, gsl::dynamic_extent> block)
	{
		assert(block.size() >= sizeof(Header));
		auto const pHeader{ gsl::make_not_null(reinterpret_cast<const Header*>(block.data())) };
		assert(block.size() >= pHeader->GetHeaderLength());
		assert(block.size() >= pHeader->GetHeaderLength() + pHeader->GetData().size());

#pragma pack(push, 1)
		struct Data1 {
			char		f1_[5];

			constexpr size_t size() const noexcept { return sizeof(*this); }
		};
#pragma pack(pop)

		auto const blockdata{ pHeader->GetData() };
		auto const pData1 = gsl::make_not_null(reinterpret_cast<const Data1*>(blockdata.data()));
		assert(pHeader->flags == 0);
		assert(blockdata.size() >= sizeof(*pData1));
		assert(blockdata.size() == pData1->size());

		assert(pData1->f1_[3] == 0);
		assert(pData1->f1_[4] == 0);
		std::cout << std::hex << std::setfill('0')
			<< " f1=" << std::setw(2) << +pData1->f1_[0]
			<< ' ' << std::setw(2) << +pData1->f1_[1]
			<< ' ' << std::setw(2) << +pData1->f1_[2] << '\n';
	}

	void TraverseTag0011(_In_ gsl::span<const std::byte, gsl::dynamic_extent> block)
	{
		assert(block.size() >= sizeof(Header));
		auto const pHeader{ gsl::make_not_null(reinterpret_cast<const Header*>(block.data())) };
		assert(block.size() >= pHeader->GetHeaderLength());
		assert(block.size() >= pHeader->GetHeaderLength() + pHeader->GetData().size());

#pragma pack(push, 1)
		struct Data1 {
			uint32_t			c04;
			uint32_t			c00;
//			LocalizedStrWithLen	name;		// The name display under Find -> Extras/, only Garmin files, not POILoader or GSAK.
//			LocalizedStrWithLen	license;

			constexpr size_t size() const noexcept { return sizeof(*this); }
		};
#pragma pack(pop)

		auto blockdata{ pHeader->GetData() };
		auto const pData1 = gsl::make_not_null(reinterpret_cast<const Data1*>(blockdata.data()));
		assert(pHeader->flags == 0);
		assert(blockdata.size() >= sizeof(*pData1));
		assert(blockdata.size() >= pData1->size());
		assert(pData1->c04 == 4);
		assert(pData1->c00 == 0);

		blockdata = blockdata.subspan(pData1->size());
		auto const name = gsl::make_not_null(reinterpret_cast<const LocalizedStrWithLen *>(blockdata.data()));
		assert(blockdata.size() >= sizeof *name);
		assert(blockdata.size() >= name->GetTotalLength());
		name->Check();

		blockdata = blockdata.subspan(name->GetTotalLength());
		auto const license = gsl::make_not_null(reinterpret_cast<const LocalizedStrWithLen*>(blockdata.data()));
		assert(blockdata.size() >= sizeof * license);
		assert(blockdata.size() == license->GetTotalLength());
		license->Check();

		std::cout << " name=" << *name
			<< " license=" << *license << '\n';
	}

	void TraverseTagFFFF(_In_ gsl::span<const std::byte, gsl::dynamic_extent> block)
	{
		assert(block.size() >= sizeof(Header));
		auto const pHeader{ gsl::make_not_null(reinterpret_cast<const Header*>(block.data())) };
		assert(block.size() >= pHeader->GetHeaderLength());
		assert(block.size() >= pHeader->GetHeaderLength() + pHeader->GetData().size());

		auto const blockdata{ pHeader->GetData() };
		assert(blockdata.size() == 0);
		std::cout << '\n';
	}

	void TraverseHeaders(_In_ gsl::span<const std::byte, gsl::dynamic_extent> block)
	{
		while(!block.empty()) {
			assert(block.size() >= sizeof(Header));
			auto const pHeader{ gsl::make_not_null(reinterpret_cast<const Header*>(block.data())) };
			assert(block.size() >= pHeader->GetHeaderLength());
			auto const subblock{ pHeader->AsSpan() };
			assert(block.size() >= subblock.size());
			std::cout << std::hex << std::setfill('0')
				<< GetPrefixString()
				<< std::setw(4) << pHeader->tag
				<< ' ' << std::setw(4) << pHeader->flags
				<< " len=" << std::setw(8) << pHeader->GetData().size();
			if (pHeader->flags == 0x0008)
			{
				std::cout << " len2=" << std::setw(8) << pHeader->GetLen2();
			}

			assert(pHeader->flags == 0x0000  ||  pHeader->flags == 0x0008);

			switch (pHeader->tag)
			{
			case 0x0000:	TraverseTag0000(subblock);	break;
			case 0x0001:	TraverseTag0001(subblock);	break;
			case 0x0002:	TraverseTag0002(subblock);	break;
			case 0x0003:	TraverseTag0003(subblock);	break;
			case 0x0004:	TraverseTag0004(subblock);	break;
			case 0x0005:	TraverseTag0005(subblock);	break;
			case 0x0006:	TraverseTag0006(subblock);	break;
			case 0x0007:	TraverseTag0007(subblock);	break;
			case 0x0008:	TraverseTag0008(subblock);	break;
			case 0x0009:	TraverseTag0009(subblock);	break;
			case 0x000A:	TraverseTag000A(subblock);	break;
			case 0x000B:	TraverseTag000B(subblock);	break;
			case 0x000C:	TraverseTag000C(subblock);	break;
			case 0x000D:	TraverseTag000D(subblock);	break;
			case 0x000E:	TraverseTag000E(subblock);	break;
			case 0x000F:	TraverseTag000F(subblock);	break;
			case 0x0011:	TraverseTag0011(subblock);	break;
			case 0xFFFF:	TraverseTagFFFF(subblock);	break;
			default:		assert(false);				break;
			}

			block = block.subspan(subblock.size());
		}
	}
} // namespace

int _tmain(_In_ int argc, _In_count_(argc) _Pre_z_ TCHAR ** argv)
{
	std::ios_base::sync_with_stdio(false);

	auto const args{ gsl::span<TCHAR*, gsl::dynamic_extent>{argv, gsl::narrow<size_t>(argc)}.subspan(1) };
	for (auto const &arg : args) {
		std::wcout << "Processing file " << arg << '\n';
		std::basic_ifstream<std::byte> infile(arg, std::basic_ifstream<std::byte>::in | std::basic_ifstream<std::byte>::binary);
		std::vector<std::byte> data((std::istreambuf_iterator<std::byte>(infile)), std::istreambuf_iterator<std::byte>());
		infile.close();

		TraverseHeaders(data);
	}

	return 0;
}

/*
Garmin POIs:
	0000
	..000f
	0001
	..0011		// Name under Extras/ (only this entry is needed for display under Extras/ instead of Extras/"User POIs")
	0009+
	..0008+
	...0008?
	....0002+
	......0006
	......000b
	......000c
	......000e
	..0007 0008
	....0004
	..0005
	ffff

POILoader:
	0000
	0001
	0009+
	..0008		// 0008 is for grouping, groups can be placed inside groups (seen up to depth 4x 0008)
	....0002+
	......0003?
	......0004
	......0006?		// Only present if folders are present
	......000a?		// gpx cmt, or gpx desc if cmt is not present
	......000b?
	......000e?		// gpx desc if gpx cmt is present
	..0007* 0000	// For each folder
	..0005+
	ffff

GSAK:
	0000
	0001
	0009+
	..0008
	....0008
	......0002+
	........0004
	........000a
	..0005
	ffff
*/

/* POLoader strategy for splitting up a POI file into different 0008 groupings:
	Start with add_group(top most bounding rect)
	add_group(rect) {
		count = number of POIs inside rect;
		if(!count) {
			return;
		}
		add a 0008 entry;
		if(count <= 510) {
			add alle POIs inside rect to the current group;
		}
		else if(count >= 516) {
			for(quarter : all four quarters of the rect) {	// split into half vertically and horizontally
				add_group(quarter)
			}
		}
		else {
			Don't know exactly.
		}
	}

	The Garmin POI files seem to have other limits, they have at most 128 entries per 0008 group and the 0008 groups can overlap.

	If a group contains only one element, then ...?
*/
